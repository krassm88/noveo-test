require 'rails_helper'

RSpec.describe Api::UsersController, type: :controller do
  describe '#create' do
    context 'valid data' do
      before do
        post :create, format: :json, params: { user: attributes_for(:user) }
      end

      it 'return 200 status' do
        expect(response).to have_http_status 200
      end
    end

    context 'invalid data' do
      before do
        post :create, format: :json, params: { user: attributes_for(:user, :invalid) }
      end

      it 'return 400 status' do
        expect(response).to have_http_status 400
      end

      it 'response have errors key' do
        expect(json_response.keys).to include('errors')
      end
    end    
  end
end
