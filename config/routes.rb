Rails.application.routes.draw do
  namespace :api do
    post :sign_up, to: 'users#create'
    post 'user_token' => 'user_token#create'
  end
end
