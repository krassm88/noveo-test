require 'rails_helper'

RSpec.describe Api::UserTokenController, type: :controller do
  describe '#create' do
    context 'valid data' do
      let!(:user) { create(:user) }

      before do
        post :create, format: :json, params: { auth: { email: user.email, password: user.password } } 
      end

      it 'return 200 status' do
        expect(response).to have_http_status 200
      end

      it 'response have jwt key' do
        expect(json_response.keys).to include('jwt')
      end
    end

    context 'invalid data' do
      let(:invalid_user) { attributes_for(:user) }

      before do
        post :create, format: :json, params: { auth: { email: invalid_user[:email], password: invalid_user[:password] } } 
      end

      it 'return 404 status' do
        expect(response).to have_http_status 404
      end
    end
  end
end
