FactoryBot.define do
  factory :user do
    email { FFaker::Internet.email }
    password 'qwerty123'
    password_confirmation 'qwerty123'

    trait :invalid do
      email ''
      password ''
      password_confirmation ''
    end
  end
end
