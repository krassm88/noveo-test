class Api::UserTokenController < Knock::AuthTokenController
  skip_before_action :verify_authenticity_token, raise: false

  def create
    render json: auth_token, status: 200
  end
end
